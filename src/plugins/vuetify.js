import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
	iconfont: 'mdi',
	theme: {
		themes: {
			light: {
				primary: colors.blue.darken2,
				secondary: colors.blue.lighten1,
				accent: '#eee',
			},
		},
	},
});
