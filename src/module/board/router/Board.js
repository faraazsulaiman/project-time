import BoardPage from '../BoardPage';

export default {
	path: '/',
	name: 'board',
	component: BoardPage
}
