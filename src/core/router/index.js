import Vue from 'vue';
import Router from 'vue-router';
import Board from '../../module/board/router/Board';

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		Board
	],
});
